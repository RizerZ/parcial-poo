﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial_1_POO
{
    public class Equipo
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public Torneo Tournment { get; set; }
        public List<Participante> Participantes { get; set; }

        public Equipo()
        {
            Participantes = new List<Participante>();
        }

        public Equipo(string _codigo, string _nombre):this()
        {
            this.Codigo = _codigo;
            this.Nombre = _nombre;
        }

        private void AgregarParticipante(Participante agregar)
        {
            
            try
            {
                if (!Participantes.Exists(x => x.DNI == agregar.DNI))
                {
                    if (agregar.Socio == true)
                    {
                        Socio a = new Socio(agregar);
                        Participantes.Add(a);
                    }
                    if (agregar.Socio == false)
                    {
                        NoSocio a = new NoSocio(agregar);
                        Participantes.Add(a);
                    }
                }
                else
                {
                    throw new Exception("DNI REPETIDO EN EL EQUIPO");
                }
               
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            

        }

        public int CantidadParticipantes()
        {
            return Participantes.Count;
        }

        public decimal Recaudacion()
        {
            decimal recaudacion = 0;
            foreach (Participante p in Participantes)
            {
                
                if(p.Socio==true)
                {
                    recaudacion+=this.Tournment.Costo-((this.Tournment.Costo * 20) / 100);
                }
                else
                {
                    recaudacion += this.Tournment.Costo;
                    
                }
            }

            return recaudacion;
        }

        /*
        public List<Participante> Participantes(Participante agregar)
        {
            get { return _participantes; }
            set { _participantes.Add(agregar); }
        }
        */

    }
}
