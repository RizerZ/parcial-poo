﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial_1_POO
{
    public class Deporte
    {
        public string Codigo { get; set; }

        public string Nombre { get; set; }

        public List<Torneo> Torneos {get;set;}

        public Deporte()
        {
            Torneos = new List<Torneo>();
        }

        public Deporte(string _codigo, string _nombre) : this()
        {
            this.Codigo = _codigo;
            this.Nombre = _nombre;
            
        }
    }
}
