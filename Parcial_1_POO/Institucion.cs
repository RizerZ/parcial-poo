﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial_1_POO
{
    public class Institucion
    {
        public List<Torneo> Torneos;
        public List<Deporte> Deportes;
        public List<Equipo> Equipos;
        public List<Participante> Participantes;
        

        public Institucion()
        {
            Torneos = new List<Torneo>();
            Deportes = new List<Deporte>();
            Equipos = new List<Equipo>();
            Participantes = new List<Participante>();
        }

        public void AgregarDeporte(Deporte agregar)
        {
            try
            {
                if (!Deportes.Exists(x => x.Codigo == agregar.Codigo))
                {
                    Deportes.Add(agregar);
                }
                else
                {
                    throw new Exception("EL CODIGO INGRESADO YA EXISTE!");
                }


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }


        }
        public void BorrarDeporte(Deporte borrar)
        {
            try
            {
                Deportes.Remove(borrar);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        public void AgregarTorneo(Torneo agregar)
        {
            try
            {
                if (!Torneos.Exists(x => x.Codigo == agregar.Codigo))
                {
                    Torneos.Add(agregar);
                }
                else
                {
                    throw new Exception("EL CODIGO INGRESADO YA EXISTE!");
                }


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        public void BorrarTorneo(Torneo borrar)
        {
            try
            {
                Torneos.Remove(borrar);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
        public void AgregarEquipo(Equipo agregar)
        {
            if (!Equipos.Exists(x => x.Codigo == agregar.Codigo))
            {
                Equipos.Add(agregar);
            }
            else
            {
                throw new Exception("EL CODIGO INGRESADO YA EXISTE!");
            }
        }
        public void BorrarEquipo(Equipo borrar)
        {
            try
            {
                Equipos.Remove(borrar);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
        public void AgregarParticipante(Participante agregar) 
        {
            if (!Participantes.Exists(x => x.DNI == agregar.DNI))
            {
                Participantes.Add(agregar);
            }
            else
            {
                throw new Exception("EL DNI INGRESADO YA EXISTE!");
            }
        }
        public void BorrarParticipante(Participante borrar)
        {
            try
            {
                Participantes.Remove(borrar);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
    }
}
