﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_1_POO
{
    public class Socio : Participante
    {
        /*
        public Socio(string _DNI, string _Nombre, string _Apellido, Equipo _Team, bool _Socio)
        {
            this.DNI = _DNI;
            this.Nombre = _Nombre;
            this.Apellido = _Apellido;
            this.Team = _Team;
            this.Socio = _Socio;
        }*/

        public Socio(Participante hola)
        {
            this.DNI = hola.DNI;
            this.Apellido = hola.Apellido;
            this.Nombre = hola.Nombre;
            this.Socio = hola.Socio;
            this.Team = hola.Team;
        }


        public override decimal Pagar(decimal costo)
        {
            return costo;
        }
    }
}
