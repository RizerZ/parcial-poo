﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_1_POO
{
    public class ClaseVistaTI
    {
        List<ClaseVistaTI> _lcvti;
        public ClaseVistaTI()
        {
            _lcvti = new List<ClaseVistaTI>();
        }
        ClaseVistaTI(int _codigo, string _nombre, DateTime _inicio, int _cantEq,int _cantPar, string _deporte, decimal _totRec):this()
        {
            Codigo = _codigo;
            Nombre = _nombre;
            Inicio = _inicio;
            CantidadEquipos = _cantEq;
            CantidadParticipantes = _cantPar;
            Deporte = _deporte;
            TotalRecaudado = _totRec;
        }
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public DateTime Inicio { get; set; }
        public int CantidadEquipos { get; set; }
        public int CantidadParticipantes { get; set; }
        public string Deporte { get; set; }
        public decimal TotalRecaudado { get; set; }

        public List<ClaseVistaTI> RetornaLista(List<Torneo> listaTorneo)
        {
            _lcvti.Clear();
            try
            {
                foreach (Torneo t in listaTorneo)
                {
                    if (t.Fin > DateTime.Today)
                    {
                        if (t.Equipos.Count>0)
                        {
                            foreach (Equipo e in t.Equipos)
                            {
                                if (_lcvti.Exists(x => x.Codigo == t.Codigo))
                                {
                                    ClaseVistaTI auxVista = _lcvti.Find(x => x.Codigo == t.Codigo);
                                    auxVista.Nombre = t.Nombre;
                                    auxVista.Inicio = t.Inicio;
                                    auxVista.CantidadEquipos = t.Equipos.Count;
                                    auxVista.CantidadParticipantes += e.CantidadParticipantes();
                                    auxVista.Deporte = t._Deporte is null ? " " : t._Deporte.Nombre;
                                    auxVista.TotalRecaudado += e.Recaudacion();
                                }
                                else
                                {
                                    _lcvti.Add(new ClaseVistaTI(t.Codigo, t.Nombre, t.Inicio, t.Equipos.Count, e.CantidadParticipantes(), (t._Deporte is null ? " " : t._Deporte.Nombre), e.Recaudacion()));
                                }
                            }
                        }
                        else
                        {
                            _lcvti.Add(new ClaseVistaTI(t.Codigo, t.Nombre, t.Inicio, t.Equipos.Count, 0, (t._Deporte is null ? " " : t._Deporte.Nombre), 0));
                        }
                       
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }

            return _lcvti;
        }
    }
}
