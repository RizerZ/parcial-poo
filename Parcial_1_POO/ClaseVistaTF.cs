﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_1_POO
{
    public class ClaseVistaTF
    {
        List<ClaseVistaTF> _lcvtf;
        public ClaseVistaTF()
        {
            _lcvtf = new List<ClaseVistaTF>();
        }
        ClaseVistaTF(int _codigo, string _nombre, int _cantEq, int _cantPart, string _deporte, decimal _totRec):this()
        {
            Codigo = _codigo;
            Nombre = _nombre;
            CantidadEquipos = _cantEq;
            CantidadParticipantes = _cantPart;
            Deporte = _deporte;
            TotalRecaudado = _totRec;
        }
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public int CantidadEquipos {get;set;}
        public int CantidadParticipantes { get; set; }
        public string Deporte { get; set; }
        public decimal TotalRecaudado { get; set; }


        public List<ClaseVistaTF> RetornaLista(List<Torneo> listaTorneo)
        {
            _lcvtf.Clear();
            try
            {
                foreach (Torneo t in listaTorneo)
                {
                    if (t.Fin<DateTime.Today)
                    {
                        if (t.Equipos.Count>0)
                        {
                            foreach (Equipo e in t.Equipos)
                            {
                                if (_lcvtf.Exists(x => x.Codigo == t.Codigo))
                                {
                                    ClaseVistaTF auxVista = _lcvtf.Find(x => x.Codigo == t.Codigo);
                                    auxVista.Nombre = t.Nombre;
                                    auxVista.CantidadEquipos = t.Equipos.Count;
                                    auxVista.CantidadParticipantes+= e.CantidadParticipantes();
                                    auxVista.Deporte = t._Deporte is null ? " " : t._Deporte.Nombre;
                                    auxVista.TotalRecaudado += e.Recaudacion();
                                    
                                    

                                }
                                else
                                {
                                    _lcvtf.Add(new ClaseVistaTF(t.Codigo, t.Nombre, t.Equipos.Count, e.CantidadParticipantes(), (t._Deporte is null ? " " : t._Deporte.Nombre), e.Recaudacion()));
                                }
                            }
                        }
                        else
                        {
                            _lcvtf.Add(new ClaseVistaTF(t.Codigo, t.Nombre, t.Equipos.Count, t.Equipos.Count, (t._Deporte is null ? " " : t._Deporte.Nombre), 0));
                        }
                        
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
           
            return _lcvtf;
        }
    }


}
