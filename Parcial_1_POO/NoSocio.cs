﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_1_POO
{
    class NoSocio:Participante
    {


        
        public NoSocio(string _nombre, string _DNI, string _apellido, Equipo _team, bool _socio)
        {
           this.DNI = _DNI;
           this.Nombre = _nombre;
           this.Apellido = _apellido;
           this.Team = _team;
           this.Socio = _socio;
        }

        public NoSocio(Participante hola)
        {
            this.DNI = hola.DNI;
            this.Apellido = hola.Apellido;
            this.Nombre = hola.Nombre;
            this.Socio = hola.Socio;
            this.Team = hola.Team;
        }

        public override decimal Pagar(decimal costo)
        {
            decimal Total = ((costo * 20) / 100) + costo;
            return Total;
        }
    }
}
