﻿namespace Parcial_1_POO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TORNEOS = new System.Windows.Forms.DataGridView();
            this.EQUIPOS = new System.Windows.Forms.DataGridView();
            this.PARTICIPANTES = new System.Windows.Forms.DataGridView();
            this.DEPORTES = new System.Windows.Forms.DataGridView();
            this.DTF = new System.Windows.Forms.DataGridView();
            this.DTI = new System.Windows.Forms.DataGridView();
            this.Alta_T = new System.Windows.Forms.Button();
            this.Baja_T = new System.Windows.Forms.Button();
            this.Modificacion_T = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.AsigEquip_E = new System.Windows.Forms.Button();
            this.Quitar_E = new System.Windows.Forms.Button();
            this.Modificacion_E = new System.Windows.Forms.Button();
            this.Baja_E = new System.Windows.Forms.Button();
            this.Alta_E = new System.Windows.Forms.Button();
            this.QuitParAsig_P = new System.Windows.Forms.Button();
            this.AsigPart_P = new System.Windows.Forms.Button();
            this.Modificacion_P = new System.Windows.Forms.Button();
            this.Baja_P = new System.Windows.Forms.Button();
            this.Alta_P = new System.Windows.Forms.Button();
            this.QuitDepAsig_D = new System.Windows.Forms.Button();
            this.AsigDep_D = new System.Windows.Forms.Button();
            this.Modificacion_D = new System.Windows.Forms.Button();
            this.Baja_D = new System.Windows.Forms.Button();
            this.Alta_D = new System.Windows.Forms.Button();
            this.EquiposTodos = new System.Windows.Forms.RadioButton();
            this.EquiposAsignados = new System.Windows.Forms.RadioButton();
            this.ParticipantesTodos = new System.Windows.Forms.RadioButton();
            this.ParticipantesAsignados = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.TORNEOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQUIPOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PARTICIPANTES)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DEPORTES)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTI)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TORNEOS
            // 
            this.TORNEOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TORNEOS.Location = new System.Drawing.Point(65, 52);
            this.TORNEOS.Name = "TORNEOS";
            this.TORNEOS.Size = new System.Drawing.Size(349, 137);
            this.TORNEOS.TabIndex = 0;
            this.TORNEOS.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TORNEOS_CellEnter);
            this.TORNEOS.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TORNEOS_RowEnter);
            // 
            // EQUIPOS
            // 
            this.EQUIPOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EQUIPOS.Location = new System.Drawing.Point(595, 52);
            this.EQUIPOS.Name = "EQUIPOS";
            this.EQUIPOS.Size = new System.Drawing.Size(214, 137);
            this.EQUIPOS.TabIndex = 1;
            this.EQUIPOS.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.EQUIPOS_CellEnter);
            this.EQUIPOS.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.EQUIPOS_RowEnter);
            // 
            // PARTICIPANTES
            // 
            this.PARTICIPANTES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PARTICIPANTES.Location = new System.Drawing.Point(1023, 52);
            this.PARTICIPANTES.Name = "PARTICIPANTES";
            this.PARTICIPANTES.Size = new System.Drawing.Size(296, 137);
            this.PARTICIPANTES.TabIndex = 2;
            this.PARTICIPANTES.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.PARTICIPANTES_CellEnter);
            this.PARTICIPANTES.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.PARTICIPANTES_RowEnter);
            // 
            // DEPORTES
            // 
            this.DEPORTES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DEPORTES.Location = new System.Drawing.Point(65, 269);
            this.DEPORTES.Name = "DEPORTES";
            this.DEPORTES.Size = new System.Drawing.Size(214, 137);
            this.DEPORTES.TabIndex = 3;
            this.DEPORTES.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DEPORTES_RowEnter);
            // 
            // DTF
            // 
            this.DTF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DTF.Location = new System.Drawing.Point(492, 295);
            this.DTF.Name = "DTF";
            this.DTF.Size = new System.Drawing.Size(770, 137);
            this.DTF.TabIndex = 4;
            // 
            // DTI
            // 
            this.DTI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DTI.Location = new System.Drawing.Point(492, 499);
            this.DTI.Name = "DTI";
            this.DTI.Size = new System.Drawing.Size(770, 137);
            this.DTI.TabIndex = 5;
            // 
            // Alta_T
            // 
            this.Alta_T.Location = new System.Drawing.Point(65, 195);
            this.Alta_T.Name = "Alta_T";
            this.Alta_T.Size = new System.Drawing.Size(86, 38);
            this.Alta_T.TabIndex = 6;
            this.Alta_T.Text = "Alta";
            this.Alta_T.UseVisualStyleBackColor = true;
            this.Alta_T.Click += new System.EventHandler(this.Alta_T_Click);
            // 
            // Baja_T
            // 
            this.Baja_T.Location = new System.Drawing.Point(172, 195);
            this.Baja_T.Name = "Baja_T";
            this.Baja_T.Size = new System.Drawing.Size(86, 38);
            this.Baja_T.TabIndex = 7;
            this.Baja_T.Text = "Baja";
            this.Baja_T.UseVisualStyleBackColor = true;
            this.Baja_T.Click += new System.EventHandler(this.Baja_T_Click);
            // 
            // Modificacion_T
            // 
            this.Modificacion_T.Location = new System.Drawing.Point(275, 195);
            this.Modificacion_T.Name = "Modificacion_T";
            this.Modificacion_T.Size = new System.Drawing.Size(86, 38);
            this.Modificacion_T.TabIndex = 8;
            this.Modificacion_T.Text = "Modificación";
            this.Modificacion_T.UseVisualStyleBackColor = true;
            this.Modificacion_T.Click += new System.EventHandler(this.Modificacion_T_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(159, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "TORNEOS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(655, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "EQUIPOS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1140, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "PARTICIPANTES";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(129, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "DEPORTES";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(697, 272);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(390, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "DATOS CONSOLIDADOS TORNEOS FINALIZADOS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(673, 476);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(473, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "DATOS CONSOLIDADOS TORNOS POR INICIAR Y EN CURSO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(105, 492);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "TOTAL GENERAL";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(109, 526);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 56);
            this.textBox1.TabIndex = 16;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AsigEquip_E
            // 
            this.AsigEquip_E.Location = new System.Drawing.Point(492, 52);
            this.AsigEquip_E.Name = "AsigEquip_E";
            this.AsigEquip_E.Size = new System.Drawing.Size(86, 38);
            this.AsigEquip_E.TabIndex = 17;
            this.AsigEquip_E.Text = "Asignar \r\nEquipo";
            this.AsigEquip_E.UseVisualStyleBackColor = true;
            this.AsigEquip_E.Click += new System.EventHandler(this.AsigEquip_E_Click);
            // 
            // Quitar_E
            // 
            this.Quitar_E.Location = new System.Drawing.Point(492, 128);
            this.Quitar_E.Name = "Quitar_E";
            this.Quitar_E.Size = new System.Drawing.Size(86, 52);
            this.Quitar_E.TabIndex = 18;
            this.Quitar_E.Text = "Quitar\r\nEquipo\r\nAsignado";
            this.Quitar_E.UseVisualStyleBackColor = true;
            this.Quitar_E.Click += new System.EventHandler(this.Quitar_E_Click);
            // 
            // Modificacion_E
            // 
            this.Modificacion_E.Location = new System.Drawing.Point(749, 195);
            this.Modificacion_E.Name = "Modificacion_E";
            this.Modificacion_E.Size = new System.Drawing.Size(86, 38);
            this.Modificacion_E.TabIndex = 21;
            this.Modificacion_E.Text = "Modificación";
            this.Modificacion_E.UseVisualStyleBackColor = true;
            this.Modificacion_E.Click += new System.EventHandler(this.Modificacion_E_Click);
            // 
            // Baja_E
            // 
            this.Baja_E.Location = new System.Drawing.Point(646, 195);
            this.Baja_E.Name = "Baja_E";
            this.Baja_E.Size = new System.Drawing.Size(86, 38);
            this.Baja_E.TabIndex = 20;
            this.Baja_E.Text = "Baja";
            this.Baja_E.UseVisualStyleBackColor = true;
            this.Baja_E.Click += new System.EventHandler(this.Baja_E_Click);
            // 
            // Alta_E
            // 
            this.Alta_E.Location = new System.Drawing.Point(539, 195);
            this.Alta_E.Name = "Alta_E";
            this.Alta_E.Size = new System.Drawing.Size(86, 38);
            this.Alta_E.TabIndex = 19;
            this.Alta_E.Text = "Alta";
            this.Alta_E.UseVisualStyleBackColor = true;
            this.Alta_E.Click += new System.EventHandler(this.Alta_E_Click);
            // 
            // QuitParAsig_P
            // 
            this.QuitParAsig_P.Location = new System.Drawing.Point(931, 128);
            this.QuitParAsig_P.Name = "QuitParAsig_P";
            this.QuitParAsig_P.Size = new System.Drawing.Size(86, 52);
            this.QuitParAsig_P.TabIndex = 23;
            this.QuitParAsig_P.Text = "Quitar\r\nParticipante\r\nAsignado";
            this.QuitParAsig_P.UseVisualStyleBackColor = true;
            this.QuitParAsig_P.Click += new System.EventHandler(this.QuitParAsig_P_Click);
            // 
            // AsigPart_P
            // 
            this.AsigPart_P.Location = new System.Drawing.Point(931, 52);
            this.AsigPart_P.Name = "AsigPart_P";
            this.AsigPart_P.Size = new System.Drawing.Size(86, 38);
            this.AsigPart_P.TabIndex = 22;
            this.AsigPart_P.Text = "Asignar \r\nParticipante";
            this.AsigPart_P.UseVisualStyleBackColor = true;
            this.AsigPart_P.Click += new System.EventHandler(this.AsigPart_P_Click);
            // 
            // Modificacion_P
            // 
            this.Modificacion_P.Location = new System.Drawing.Point(1233, 195);
            this.Modificacion_P.Name = "Modificacion_P";
            this.Modificacion_P.Size = new System.Drawing.Size(86, 38);
            this.Modificacion_P.TabIndex = 26;
            this.Modificacion_P.Text = "Modificación";
            this.Modificacion_P.UseVisualStyleBackColor = true;
            this.Modificacion_P.Click += new System.EventHandler(this.Modificacion_P_Click);
            // 
            // Baja_P
            // 
            this.Baja_P.Location = new System.Drawing.Point(1130, 195);
            this.Baja_P.Name = "Baja_P";
            this.Baja_P.Size = new System.Drawing.Size(86, 38);
            this.Baja_P.TabIndex = 25;
            this.Baja_P.Text = "Baja";
            this.Baja_P.UseVisualStyleBackColor = true;
            this.Baja_P.Click += new System.EventHandler(this.Baja_P_Click);
            // 
            // Alta_P
            // 
            this.Alta_P.Location = new System.Drawing.Point(1023, 195);
            this.Alta_P.Name = "Alta_P";
            this.Alta_P.Size = new System.Drawing.Size(86, 38);
            this.Alta_P.TabIndex = 24;
            this.Alta_P.Text = "Alta";
            this.Alta_P.UseVisualStyleBackColor = true;
            this.Alta_P.Click += new System.EventHandler(this.Alta_P_Click);
            // 
            // QuitDepAsig_D
            // 
            this.QuitDepAsig_D.Location = new System.Drawing.Point(285, 349);
            this.QuitDepAsig_D.Name = "QuitDepAsig_D";
            this.QuitDepAsig_D.Size = new System.Drawing.Size(86, 52);
            this.QuitDepAsig_D.TabIndex = 28;
            this.QuitDepAsig_D.Text = "Quitar\r\nDeporte\r\nAsignado";
            this.QuitDepAsig_D.UseVisualStyleBackColor = true;
            this.QuitDepAsig_D.Click += new System.EventHandler(this.QuitDepAsig_D_Click);
            // 
            // AsigDep_D
            // 
            this.AsigDep_D.Location = new System.Drawing.Point(285, 273);
            this.AsigDep_D.Name = "AsigDep_D";
            this.AsigDep_D.Size = new System.Drawing.Size(86, 38);
            this.AsigDep_D.TabIndex = 27;
            this.AsigDep_D.Text = "Asignar \r\nDeporte";
            this.AsigDep_D.UseVisualStyleBackColor = true;
            this.AsigDep_D.Click += new System.EventHandler(this.AsigDep_D_Click);
            // 
            // Modificacion_D
            // 
            this.Modificacion_D.Location = new System.Drawing.Point(252, 412);
            this.Modificacion_D.Name = "Modificacion_D";
            this.Modificacion_D.Size = new System.Drawing.Size(86, 38);
            this.Modificacion_D.TabIndex = 31;
            this.Modificacion_D.Text = "Modificación";
            this.Modificacion_D.UseVisualStyleBackColor = true;
            this.Modificacion_D.Click += new System.EventHandler(this.Modificacion_D_Click);
            // 
            // Baja_D
            // 
            this.Baja_D.Location = new System.Drawing.Point(149, 412);
            this.Baja_D.Name = "Baja_D";
            this.Baja_D.Size = new System.Drawing.Size(86, 38);
            this.Baja_D.TabIndex = 30;
            this.Baja_D.Text = "Baja";
            this.Baja_D.UseVisualStyleBackColor = true;
            this.Baja_D.Click += new System.EventHandler(this.Baja_D_Click);
            // 
            // Alta_D
            // 
            this.Alta_D.Location = new System.Drawing.Point(42, 412);
            this.Alta_D.Name = "Alta_D";
            this.Alta_D.Size = new System.Drawing.Size(86, 38);
            this.Alta_D.TabIndex = 29;
            this.Alta_D.Text = "Alta";
            this.Alta_D.UseVisualStyleBackColor = true;
            this.Alta_D.Click += new System.EventHandler(this.Alta_D_Click);
            // 
            // EquiposTodos
            // 
            this.EquiposTodos.AutoSize = true;
            this.EquiposTodos.Location = new System.Drawing.Point(5, 14);
            this.EquiposTodos.Name = "EquiposTodos";
            this.EquiposTodos.Size = new System.Drawing.Size(111, 17);
            this.EquiposTodos.TabIndex = 32;
            this.EquiposTodos.TabStop = true;
            this.EquiposTodos.Text = "Todos los equipos";
            this.EquiposTodos.UseVisualStyleBackColor = true;
            this.EquiposTodos.CheckedChanged += new System.EventHandler(this.EquiposTodos_CheckedChanged);
            // 
            // EquiposAsignados
            // 
            this.EquiposAsignados.AutoSize = true;
            this.EquiposAsignados.Location = new System.Drawing.Point(5, 37);
            this.EquiposAsignados.Name = "EquiposAsignados";
            this.EquiposAsignados.Size = new System.Drawing.Size(114, 17);
            this.EquiposAsignados.TabIndex = 33;
            this.EquiposAsignados.TabStop = true;
            this.EquiposAsignados.Text = "Equipos asignados";
            this.EquiposAsignados.UseVisualStyleBackColor = true;
            this.EquiposAsignados.CheckedChanged += new System.EventHandler(this.EquiposAsignados_CheckedChanged);
            // 
            // ParticipantesTodos
            // 
            this.ParticipantesTodos.AutoSize = true;
            this.ParticipantesTodos.Location = new System.Drawing.Point(6, 11);
            this.ParticipantesTodos.Name = "ParticipantesTodos";
            this.ParticipantesTodos.Size = new System.Drawing.Size(134, 17);
            this.ParticipantesTodos.TabIndex = 34;
            this.ParticipantesTodos.TabStop = true;
            this.ParticipantesTodos.Text = "Todos los participantes";
            this.ParticipantesTodos.UseVisualStyleBackColor = true;
            this.ParticipantesTodos.CheckedChanged += new System.EventHandler(this.ParticipantesTodos_CheckedChanged);
            // 
            // ParticipantesAsignados
            // 
            this.ParticipantesAsignados.AutoSize = true;
            this.ParticipantesAsignados.Location = new System.Drawing.Point(6, 38);
            this.ParticipantesAsignados.Name = "ParticipantesAsignados";
            this.ParticipantesAsignados.Size = new System.Drawing.Size(137, 17);
            this.ParticipantesAsignados.TabIndex = 35;
            this.ParticipantesAsignados.TabStop = true;
            this.ParticipantesAsignados.Text = "Participantes asignados";
            this.ParticipantesAsignados.UseVisualStyleBackColor = true;
            this.ParticipantesAsignados.CheckedChanged += new System.EventHandler(this.ParticipantesAsignados_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EquiposTodos);
            this.groupBox1.Controls.Add(this.EquiposAsignados);
            this.groupBox1.Location = new System.Drawing.Point(811, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(120, 64);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ParticipantesTodos);
            this.groupBox2.Controls.Add(this.ParticipantesAsignados);
            this.groupBox2.Location = new System.Drawing.Point(1322, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 61);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1461, 672);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Modificacion_D);
            this.Controls.Add(this.Baja_D);
            this.Controls.Add(this.Alta_D);
            this.Controls.Add(this.QuitDepAsig_D);
            this.Controls.Add(this.AsigDep_D);
            this.Controls.Add(this.Modificacion_P);
            this.Controls.Add(this.Baja_P);
            this.Controls.Add(this.Alta_P);
            this.Controls.Add(this.QuitParAsig_P);
            this.Controls.Add(this.AsigPart_P);
            this.Controls.Add(this.Modificacion_E);
            this.Controls.Add(this.Baja_E);
            this.Controls.Add(this.Alta_E);
            this.Controls.Add(this.Quitar_E);
            this.Controls.Add(this.AsigEquip_E);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Modificacion_T);
            this.Controls.Add(this.Baja_T);
            this.Controls.Add(this.Alta_T);
            this.Controls.Add(this.DTI);
            this.Controls.Add(this.DTF);
            this.Controls.Add(this.DEPORTES);
            this.Controls.Add(this.PARTICIPANTES);
            this.Controls.Add(this.EQUIPOS);
            this.Controls.Add(this.TORNEOS);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TORNEOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EQUIPOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PARTICIPANTES)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DEPORTES)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTI)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TORNEOS;
        private System.Windows.Forms.DataGridView EQUIPOS;
        private System.Windows.Forms.DataGridView PARTICIPANTES;
        private System.Windows.Forms.DataGridView DEPORTES;
        private System.Windows.Forms.DataGridView DTF;
        private System.Windows.Forms.DataGridView DTI;
        private System.Windows.Forms.Button Alta_T;
        private System.Windows.Forms.Button Baja_T;
        private System.Windows.Forms.Button Modificacion_T;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button AsigEquip_E;
        private System.Windows.Forms.Button Quitar_E;
        private System.Windows.Forms.Button Modificacion_E;
        private System.Windows.Forms.Button Baja_E;
        private System.Windows.Forms.Button Alta_E;
        private System.Windows.Forms.Button QuitParAsig_P;
        private System.Windows.Forms.Button AsigPart_P;
        private System.Windows.Forms.Button Modificacion_P;
        private System.Windows.Forms.Button Baja_P;
        private System.Windows.Forms.Button Alta_P;
        private System.Windows.Forms.Button QuitDepAsig_D;
        private System.Windows.Forms.Button AsigDep_D;
        private System.Windows.Forms.Button Modificacion_D;
        private System.Windows.Forms.Button Baja_D;
        private System.Windows.Forms.Button Alta_D;
        private System.Windows.Forms.RadioButton EquiposTodos;
        private System.Windows.Forms.RadioButton EquiposAsignados;
        private System.Windows.Forms.RadioButton ParticipantesTodos;
        private System.Windows.Forms.RadioButton ParticipantesAsignados;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

