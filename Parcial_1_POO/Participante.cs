﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial_1_POO
{
    public class Participante
    {
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public Equipo Team { get; set; }
        public bool Socio  { get; set; }

        /*public Participante(string _nombre, string _DNI, string _apellido, Equipo _team, bool _socio)
        {
            this.DNI = _DNI;
            this.Nombre = _nombre;
            this.Apellido = _apellido;
            this.Team = _team;
            this.Socio = _socio;
        }*/
        public Participante()
        {

        }

        public Participante(string _DNI, string _Nombre, string _Apellido, bool _Socio)
        {
            this.DNI = _DNI;
            this.Nombre = _Nombre;
            this.Apellido = _Apellido;
            this.Socio = _Socio;
        }

        public virtual decimal Pagar(decimal costo)
        {
            return costo;
        }



    }
}
