﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial_1_POO
{
    public class Torneo
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public List<Equipo> Equipos { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public decimal Costo { get; set; }
        private Deporte _deporte;

        public Deporte _Deporte
        {
            set { _deporte = value; }
            get { return _deporte; }
        }




        //public Deporte _Deporte { get; set; }

        public Torneo()
        {
            Equipos = new List<Equipo>();
        }
        public Torneo(int _codigo, string _nombre, DateTime _inicio, DateTime _fin, decimal _costo):this()
        {
            this.Codigo = _codigo;
            this.Nombre = _nombre;
            this.Inicio = _inicio;
            this.Fin = _fin;
            this.Costo = _costo;
        }

        public decimal TotalRecaudado()
        {
            decimal Total = 0;            
            foreach (var cosa in Equipos)
            {
                foreach (var cosita in cosa.Participantes)
                {
                    Total += cosita.Pagar(this.Costo);

                }
            }
            return Total;
        }
        public int TotalEquipos()
        {
            return Equipos.Count;
        }
        public void AsignarEquipo(Equipo agregar)
        {
            //revisar codigo llegado el momento
            try
            {
                if (!Equipos.Exists(x => x.Codigo == agregar.Codigo))
                {
                    Equipos.Add(agregar);
                }
                else
                {
                    throw new Exception("EL EQUIPO INGRESADO YA EXISTE!");
                }


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            
        }
        public void DesasignarEquipo(Equipo borrar)
        {
            //revisar codigo llegado el momento
            try
            {
                Equipos.Remove(borrar);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
    }
}
