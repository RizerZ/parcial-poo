﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;

namespace Parcial_1_POO
{
    public partial class Form1 : Form
    {
        private Institucion _institucion;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                _institucion = new Institucion();
                TORNEOS.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                TORNEOS.MultiSelect = false;
                EQUIPOS.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                EQUIPOS.MultiSelect = false;
                PARTICIPANTES.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                PARTICIPANTES.MultiSelect = false;
                DEPORTES.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                DEPORTES.MultiSelect = false;
                ParticipantesTodos.Checked = true;
                EquiposTodos.Checked = true;
                Baja_T.Visible = false;
                OcultarBotones(Baja_T, Modificacion_T, Baja_D, Modificacion_D, Baja_E, Modificacion_E, Baja_P, Modificacion_P, Quitar_E, QuitDepAsig_D, QuitParAsig_P);


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        private void Alta_T_Click(object sender, EventArgs e)
        {
            try
            {
                _institucion.AgregarTorneo(new Torneo(int.Parse(Interaction.InputBox("Codigo: ")),
                                                      Interaction.InputBox("Nombre: "),
                                                      DateTime.Parse(Interaction.InputBox("Fecha de inicio: ")),
                                                      DateTime.Parse(Interaction.InputBox("Fecha de finalización")),
                                                      decimal.Parse(Interaction.InputBox("Costo: "))));
                Vista();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            TORNEOS_CellEnter(null, null);
            Mostrar(TORNEOS, _institucion.Torneos);
            MostrarBotones(Baja_T, Modificacion_T);
        }

        private void MostrarBotones(Button a, Button b)
        {
            a.Visible = true;
            b.Visible = true;
        }

        private void Baja_T_Click(object sender, EventArgs e)
        {
            try
            {
                Torneo auxTorneo = (Torneo)TORNEOS.SelectedRows[0].DataBoundItem;
                if (auxTorneo.TotalEquipos() > 0)
                {
                    foreach (Equipo a in auxTorneo.Equipos)
                    {
                        a.Tournment = null;
                    }
                }
                if (EquiposAsignados.Checked == true)
                {
                    EquiposAsignados.Checked = false;
                    EquiposTodos.Checked = true;
                }
                TORNEOS_CellEnter(null, null);
                EQUIPOS_CellEnter(null, null);
                _institucion.BorrarTorneo(auxTorneo);
                Mostrar(TORNEOS, _institucion.Torneos);
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            if (_institucion.Torneos.Count == 0)
            {
                OcultarBotones(Baja_T, Modificacion_T);
            }

        }
        private void OcultarBotones(Button ocultar1, Button ocultar2)
        {
            ocultar1.Visible = false; ocultar2.Visible = false;
        }
        private void OcultarBotones(Button ocultar1, Button ocultar2, Button ocultar3, Button ocultar4, Button ocultar5, Button ocultar6, Button ocultar7, Button ocultar8, Button ocultar9, Button ocultar10, Button ocultar11)
        {
            { ocultar1.Visible = false; ocultar2.Visible = false; ocultar3.Visible = false; ocultar4.Visible = false; ocultar5.Visible = false; ocultar6.Visible = false; ocultar7.Visible = false; ocultar8.Visible = false; ocultar9.Visible = false; ocultar10.Visible = false; ocultar11.Visible = false; }
        }

        private void Modificacion_T_Click(object sender, EventArgs e)
        {
            try
            {
                Torneo auxTorneo = (((Torneo)TORNEOS.SelectedRows[0].DataBoundItem));
                if (!(auxTorneo is null))
                {
                    IngresaDatos(auxTorneo);
                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            Mostrar(TORNEOS, _institucion.Torneos);
            if (_institucion.Torneos.Count == 0) { Baja_T.Visible = false; }
        }

        private void IngresaDatos(Torneo auxTorneo)
        {


            auxTorneo.Nombre = Interaction.InputBox("Nombre: ", "", auxTorneo.Nombre);
            auxTorneo.Inicio = DateTime.Parse(Interaction.InputBox("Inicio: ", "", auxTorneo.Inicio.ToString()));
            auxTorneo.Fin = DateTime.Parse(Interaction.InputBox("Fin: ", "", auxTorneo.Fin.ToString()));
            auxTorneo.Costo = Decimal.Parse(Interaction.InputBox("Costo: ", "", auxTorneo.Costo.ToString()));
        }
        private void IngresaDatos(Equipo auxEquipo)
        {
            auxEquipo.Nombre = Interaction.InputBox("Nombre: ", "", auxEquipo.Nombre);
        }
        private void IngresaDatos(Deporte auxDeporte)
        {
            auxDeporte.Nombre = Interaction.InputBox("Nombre: ", "", auxDeporte.Nombre);
        }

        private void IngresaDatos(Participante auxParticipante)
        {
            auxParticipante.Nombre = Interaction.InputBox("Nombre: ", "", auxParticipante.Nombre);
            auxParticipante.Apellido = Interaction.InputBox("Apellido: ", "", auxParticipante.Apellido);
            auxParticipante.Socio = bool.Parse(Interaction.InputBox("¿Es socio? True or False", "", auxParticipante.Socio.ToString()));
        }

        private void Mostrar(DataGridView dgv, object quemuestro)
        {
            dgv.DataSource = null;
            dgv.DataSource = quemuestro;
        }

        private void Alta_E_Click(object sender, EventArgs e)
        {
            try
            {
                _institucion.AgregarEquipo(new Equipo(Interaction.InputBox("Codigo: "),
                                                      Interaction.InputBox("Nombre: ")));
                Mostrar(EQUIPOS, _institucion.Equipos);
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            TORNEOS_CellEnter(null, null);//creo q no sirve
            EQUIPOS_CellEnter(null, null);

            MostrarBotones(Baja_E, Modificacion_E);
            if (EQUIPOS.Rows.Count == 0)
            {
                Quitar_E.Visible = false;
            }
            else
            {
                Quitar_E.Visible = true;
            }
        }

        private void Baja_E_Click(object sender, EventArgs e)
        {

            try
            {
                Equipo auxEquipo = (Equipo)EQUIPOS.SelectedRows[0].DataBoundItem;
                if (auxEquipo.Participantes.Count > 0)
                {
                    foreach (Participante a in auxEquipo.Participantes)
                    {
                        a.Team = null;
                    }
                }
                if (auxEquipo.Tournment != null)
                {
                    auxEquipo.Tournment.Equipos.Remove(auxEquipo);
                }
                TORNEOS_CellEnter(null, null);
                EQUIPOS_CellEnter(null, null);
                _institucion.BorrarEquipo(auxEquipo);
                Mostrar(EQUIPOS, _institucion.Equipos);
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            if (_institucion.Equipos.Count == 0)
            {
                OcultarBotones(Baja_E, Modificacion_E);
            }
            if (EQUIPOS.Rows.Count == 0)
            {
                Quitar_E.Visible = false;
            }
            else
            {
                Quitar_E.Visible = true;
            }

        }

        private void Modificacion_E_Click(object sender, EventArgs e)
        {
            try
            {
                Equipo auxEquipo = (((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem));
                if (!(auxEquipo is null))
                {
                    IngresaDatos(auxEquipo);

                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            Mostrar(TORNEOS, _institucion.Torneos);
            if (_institucion.Torneos.Count == 0) { Baja_T.Visible = false; }
        }

        private void Alta_P_Click(object sender, EventArgs e)
        {
            try
            {
                _institucion.AgregarParticipante(new Participante(Interaction.InputBox("DNI: "),
                                                      Interaction.InputBox("Nombre: "),
                                                      Interaction.InputBox("Apellido: "),
                                                      bool.Parse(Interaction.InputBox("¿Es socio? True or False"))));
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            TORNEOS_CellEnter(null, null);
            EQUIPOS_CellEnter(null, null);
            Mostrar(PARTICIPANTES, _institucion.Participantes);
            MostrarBotones(Baja_P, Modificacion_P);
            if (PARTICIPANTES.Rows.Count == 0)
            {
                QuitParAsig_P.Visible = false;
            }
            else
            {
                QuitParAsig_P.Visible = true;
            }
        }

        private void Baja_P_Click(object sender, EventArgs e)
        {

            try
            {
                Participante auxParticipante = (Participante)PARTICIPANTES.SelectedRows[0].DataBoundItem;
                if (auxParticipante.Team != null)
                {
                    auxParticipante.Team.Participantes.Remove(auxParticipante);
                }
                //TORNEOS_CellEnter(null, null);
                EQUIPOS_CellEnter(null, null);
                _institucion.BorrarParticipante(auxParticipante);
                if (ParticipantesAsignados.Checked == true)
                {
                    Mostrar(PARTICIPANTES, ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem).Participantes.ToList<Participante>());
                }
                else
                {
                    Mostrar(PARTICIPANTES, _institucion.Participantes);
                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            if (_institucion.Participantes.Count == 0)
            {
                OcultarBotones(Baja_P, Modificacion_P);
            }
            if (PARTICIPANTES.Rows.Count == 0)
            {
                QuitParAsig_P.Visible = false;
            }
            else
            {
                QuitParAsig_P.Visible = true;
            }
        }

        private void Modificacion_P_Click(object sender, EventArgs e)
        {
            try
            {

                Participante auxParticipante = (((Participante)PARTICIPANTES.SelectedRows[0].DataBoundItem));
                if (!(auxParticipante is null))
                {
                    IngresaDatos(auxParticipante);

                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            Mostrar(PARTICIPANTES, _institucion.Participantes);
            if (_institucion.Participantes.Count == 0) { Baja_P.Visible = false; }
        }

        private void Alta_D_Click(object sender, EventArgs e)
        {
            try
            {
                _institucion.AgregarDeporte(new Deporte(Interaction.InputBox("Codigo: "),
                                                      Interaction.InputBox("Nombre: ")));
                Mostrar(DEPORTES, _institucion.Deportes);
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            TORNEOS_CellEnter(null, null);
            //EQUIPOS_CellEnter(null, null);

            MostrarBotones(Baja_D, Modificacion_D);
            if (DEPORTES.Rows.Count == 0)
            {
                QuitDepAsig_D.Visible = false;
            }
            else
            {
                QuitDepAsig_D.Visible = true;
            }
        }

        private void Baja_D_Click(object sender, EventArgs e)
        {
            try
            {
                Deporte auxDeporte = (Deporte)DEPORTES.SelectedRows[0].DataBoundItem;
                if (DEPORTES.Rows.Count > 0 && TORNEOS.Rows.Count > 0)
                {
                    Torneo auxTorneo = auxDeporte.Torneos.Find(x => x._Deporte.Codigo == auxDeporte.Codigo);
                    foreach (Torneo a in auxDeporte.Torneos)
                    {
                        a._Deporte = null;
                    }
                    auxTorneo._Deporte = null;
                    _institucion.Deportes.Remove(auxDeporte);


                }
                else
                {
                    _institucion.Deportes.Remove(auxDeporte);
                }
                TORNEOS_CellEnter(null, null);
                //EQUIPOS_CellEnter(null, null);
                Mostrar(DEPORTES, _institucion.Deportes);
                Vista();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            if (_institucion.Deportes.Count == 0)
            {
                OcultarBotones(Baja_D, Modificacion_D);
            }
            if (EQUIPOS.Rows.Count == 0)
            {
                QuitDepAsig_D.Visible = false;
            }
            else
            {
                QuitDepAsig_D.Visible = true;
            }

        }

        private void Modificacion_D_Click(object sender, EventArgs e)
        {
            try
            {

                Deporte auxDeporte = (((Deporte)DEPORTES.SelectedRows[0].DataBoundItem));
                if (!(auxDeporte is null))
                {
                    IngresaDatos(auxDeporte);

                }
                Vista();



            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            Mostrar(DEPORTES, _institucion.Deportes);
            if (_institucion.Deportes.Count == 0) { Baja_D.Visible = false; }
        }

        private void AsigEquip_E_Click(object sender, EventArgs e)
        {
            try
            {
                if (TORNEOS.Rows.Count > 0 && EQUIPOS.Rows.Count > 0)
                {
                    Torneo auxTorneo;
                    Equipo auxEquipo;
                    auxTorneo = ((Torneo)TORNEOS.SelectedRows[0].DataBoundItem);
                    auxEquipo = ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem);
                    if (auxEquipo.Tournment is null)
                    {
                        auxTorneo.AsignarEquipo(auxEquipo);
                        auxEquipo.Tournment = auxTorneo;
                    }
                    else
                    {
                        throw new Exception("El equipo ya está participando en un torneo");
                    }

                }
                else
                {
                    throw new Exception("Alguna de las grillas no posee elementos");
                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        private void EquiposTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (_institucion.Equipos.Count == 0)
            {
                EQUIPOS.DataSource = null;
            }
            else
            {
                Mostrar(EQUIPOS, _institucion.Equipos);
            }
            TORNEOS_CellEnter(null, null);
            EQUIPOS_CellEnter(null, null);
        }

        private void EquiposAsignados_CheckedChanged(object sender, EventArgs e)
        {
            if (TORNEOS.Rows.Count > 0)
            {
                Mostrar(EQUIPOS, ((Torneo)TORNEOS.SelectedRows[0].DataBoundItem).Equipos);
            }
            else
            {
                EQUIPOS.DataSource = null;
            }
            TORNEOS_CellEnter(null, null);
            EQUIPOS_CellEnter(null, null);
        }

        private void ParticipantesTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (_institucion.Participantes.Count == 0)
            {
                PARTICIPANTES.DataSource = null;
            }
            else
            {
                Mostrar(PARTICIPANTES, _institucion.Participantes);
            }
            //TORNEOS_CellEnter(null, null);
            EQUIPOS_CellEnter(null, null);
        }

        private void ParticipantesAsignados_CheckedChanged(object sender, EventArgs e)
        {
            if (EQUIPOS.Rows.Count > 0)
            {
                Mostrar(PARTICIPANTES, ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem).Participantes.ToList<Participante>());
            }
            else
            {
                EQUIPOS.DataSource = null;
            }
            //TORNEOS_CellEnter(null, null);
            EQUIPOS_CellEnter(null, null);
        }

        private void TORNEOS_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (TORNEOS.Rows.Count > 0 && EquiposAsignados.Checked == true)
                {
                    Mostrar(EQUIPOS, ((Torneo)TORNEOS.SelectedRows[0].DataBoundItem).Equipos.ToList<Equipo>());
                }
                else
                {
                    Mostrar(EQUIPOS, _institucion.Equipos.ToList<Equipo>());
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        private void EQUIPOS_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (EQUIPOS.Rows.Count > 0 && ParticipantesAsignados.Checked == true)
                {
                    Mostrar(PARTICIPANTES, ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem).Participantes.ToList<Participante>());
                }
                else
                {
                    Mostrar(PARTICIPANTES, _institucion.Participantes.ToList<Participante>());
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void Quitar_E_Click(object sender, EventArgs e)
        {
            try
            {

                if (TORNEOS.Rows.Count > 0 && EQUIPOS.Rows.Count > 0)
                {
                    Torneo auxTorneo = (Torneo)TORNEOS.SelectedRows[0].DataBoundItem;
                    string codigo = EQUIPOS.SelectedRows[0].Cells[0].Value.ToString();
                    Equipo auxEquipo = auxTorneo.Equipos.Find(x => x.Codigo == codigo);

                    auxTorneo.Equipos.Remove(auxEquipo);
                    if (auxEquipo.Tournment != null)
                    {
                        auxEquipo.Tournment = null;
                    }

                    TORNEOS_CellEnter(null, null);
                    EQUIPOS_CellEnter(null, null);
                    if (EQUIPOS.Rows.Count == 0)
                    {
                        Quitar_E.Visible = false;
                    }
                }
                else
                {
                    throw new Exception("No hay Equipos o Torneos");
                }
                Vista();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }


        }

        private void PARTICIPANTES_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                /*
                if(PARTICIPANTES.Rows.Count>0&&ParticipantesAsignados.Checked==true)
                {
                    Mostrar(PARTICIPANTES, ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem).Participantes);
                }
                else
                {
                    Mostrar(PARTICIPANTES, _institucion.Participantes.ToList<Participante>());
                }
                */
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void AsigPart_P_Click(object sender, EventArgs e)
        {
            try
            {
                if (EQUIPOS.Rows.Count > 0 && PARTICIPANTES.Rows.Count > 0)
                {
                    Equipo auxEquipo;
                    Participante auxParticipante;
                    auxEquipo = ((Equipo)EQUIPOS.SelectedRows[0].DataBoundItem);
                    auxParticipante = ((Participante)PARTICIPANTES.SelectedRows[0].DataBoundItem);
                    if (auxParticipante.Team is null)
                    {
                        if (!auxEquipo.Participantes.Exists((x => x.DNI == auxParticipante.DNI)))
                        {
                            auxEquipo.Participantes.Add(auxParticipante);
                            auxParticipante.Team = auxEquipo;
                        }
                        else
                        {
                            throw new Exception("El jugador ya está en el equipo");
                        }
                    }
                    else
                    {
                        throw new Exception("El jugador ya está en un equipo");
                    }
                }
                else
                {
                    throw new Exception("Alguna de las grillas no posee elementos");
                }
                Vista();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void QuitParAsig_P_Click(object sender, EventArgs e)
        {
            try
            {

                if (EQUIPOS.Rows.Count > 0 && PARTICIPANTES.Rows.Count > 0)
                {
                    Equipo auxEquipo = (Equipo)EQUIPOS.SelectedRows[0].DataBoundItem;
                    string _DNI = PARTICIPANTES.SelectedRows[0].Cells[0].Value.ToString();
                    Participante auxParticipante = auxEquipo.Participantes.Find(x => x.DNI == _DNI);
                    auxEquipo.Participantes.Remove(auxParticipante);
                    if (auxParticipante.Team != null)
                    {
                        auxParticipante.Team = null;
                    }

                    //TORNEOS_CellEnter(null, null);
                    EQUIPOS_CellEnter(null, null);
                    if (PARTICIPANTES.Rows.Count == 0)
                    {
                        QuitParAsig_P.Visible = false;
                    }
                }
                else
                {
                    throw new Exception("No hay Participantes o Equipos");
                }
                Vista();


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        private void AsigDep_D_Click(object sender, EventArgs e)
        {
            try
            {
                if (DEPORTES.Rows.Count > 0 && TORNEOS.Rows.Count > 0)
                {
                    Deporte auxDeporte;
                    Torneo auxTorneo;
                    auxDeporte = ((Deporte)DEPORTES.SelectedRows[0].DataBoundItem);
                    auxTorneo = ((Torneo)TORNEOS.SelectedRows[0].DataBoundItem);
                    if (auxTorneo._Deporte is null)
                    {
                        auxDeporte.Torneos.Add(auxTorneo);
                        auxTorneo._Deporte = auxDeporte;
                    }
                    else
                    {

                        throw new Exception("El torneo ya tiene un deporte");

                    }
                    Vista();
                }
                else
                {
                    throw new Exception("Alguna de las grillas no posee elementos");
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void QuitDepAsig_D_Click(object sender, EventArgs e)
        {
            try
            {

                if (DEPORTES.Rows.Count > 0 && TORNEOS.Rows.Count > 0)
                {
                    Deporte auxDeporte = (Deporte)DEPORTES.SelectedRows[0].DataBoundItem;
                    string codigo = DEPORTES.SelectedRows[0].Cells[0].Value.ToString();
                    Torneo auxTorneo = auxDeporte.Torneos.Find(x => x._Deporte.Codigo == codigo);
                    auxDeporte.Torneos.Remove(auxTorneo);
                    auxTorneo._Deporte = null;
                    TORNEOS_CellEnter(null, null);
                    //EQUIPOS_CellEnter(null, null);
                    if (EQUIPOS.Rows.Count == 0)
                    {
                        Quitar_E.Visible = false;
                    }
                    Vista();
                }
                else
                {
                    throw new Exception("No hay Equipos o Torneos");
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void TORNEOS_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            Vista();
        }


        private void EQUIPOS_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

            Vista();
        }

        private void PARTICIPANTES_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

            Vista();
        }

        private void DEPORTES_RowEnter(object sender, DataGridViewCellEventArgs e)
        {

            Vista();
        }
        private void Vista()
        {
            ClaseVistaTF _tf = new ClaseVistaTF();
            ClaseVistaTI _ti = new ClaseVistaTI();
            try
            {
                decimal total = 0;
                Mostrar(DTF, _tf.RetornaLista(_institucion.Torneos.ToList<Torneo>()));
                Mostrar(DTI, _ti.RetornaLista(_institucion.Torneos.ToList<Torneo>()));
                foreach (ClaseVistaTF item in _tf.RetornaLista(_institucion.Torneos.ToList<Torneo>()))
                {
                    total += item.TotalRecaudado;
                }
                foreach (ClaseVistaTI item in _ti.RetornaLista(_institucion.Torneos.ToList<Torneo>()))
                {
                    total += item.TotalRecaudado;
                }
                textBox1.Text = total.ToString();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
